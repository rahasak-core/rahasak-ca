package com.score.aplos.cassandra

import java.util.Date

import com.dataoperandz.cassper.Cassper
import com.datastax.driver.core.ConsistencyLevel
import com.score.aplos.util.{AppLogger, CryptoFactory}


case class Cert(id: String, pubkey: String, digsig: String, timestamp: Date = new Date)

case class Trans(id: String, execer: String, actor: String, messageTyp: String, message: String, digsig: String, timestamp: Date = new Date)

object CassandraStore extends CassandraCluster with AppLogger {

  // transaction
  lazy val dsps = session.prepare("SELECT id FROM mystiko.trans WHERE execer = ? AND actor = ? AND id = ? LIMIT 1")
    .setConsistencyLevel(ConsistencyLevel.LOCAL_QUORUM)
  lazy val ctps = session.prepare("INSERT INTO mystiko.trans(id, execer, actor, message_typ, message, digsig, timestamp) VALUES(?, ?, ?, ?, ?, ?, ?)")
    .setConsistencyLevel(ConsistencyLevel.LOCAL_QUORUM)

  // cert
  lazy val ccps = session.prepare("INSERT INTO mystiko.certs(id, pubkey, digsig, timestamp) VALUES(?, ?, ?, ?)")
    .setConsistencyLevel(ConsistencyLevel.LOCAL_QUORUM)
  lazy val gcps = session.prepare("SELECT * FROM mystiko.certs where id = ? LIMIT 1")
    .setConsistencyLevel(ConsistencyLevel.LOCAL_QUORUM)

  def init(): Unit = {
    // create keyspace
    session.execute(cassandraKeyspaceSchema)

    // migration via cassper
    // create udts, tables
    val builder = new Cassper().build("mystiko", session)
    builder.migrate("mystiko")

    // create admin user
    val pubKey = CryptoFactory.loadRSAPublicKey()
    val digisig = CryptoFactory.sign(pubKey)
    val caCert = Cert("ca", pubKey, digisig)
    CassandraStore.createCert(caCert)
  }

  def isDoubleSpend(execer: String, actor: String, id: String): Boolean = {
    // check weather given trans with id exists
    val row = session.execute(dsps.bind(execer, actor, id)).one()
    row != null
  }

  def createTrans(trans: Trans) = {
    session.execute(ctps.bind(trans.id, trans.execer, trans.actor, trans.messageTyp, trans.message,
      trans.digsig, trans.timestamp))
  }

  def createCert(cert: Cert) = {
    session.execute(ccps.bind(cert.id, cert.pubkey, cert.digsig, cert.timestamp))
  }

  def getCert(id: String): Option[Cert] = {
    val row = session.execute(gcps.bind(id)).one()
    if (row != null) Option(Cert(row.getString("id"), row.getString("payload"), row.getString("digsig"),
      row.getTimestamp("timestamp")))
    else None
  }

}

//object M extends App {
//  CassandraStore.init()
//  println(CassandraStore.isDoubleSpend("eranga", "AccountActor", "112"))
//
//  val acc1 = Account(
//    "111", "222", "w32323", "user", List(), "pubkey", "2323", "2323", "2323", "1", "2", "3", "8736", "eranga", "78", "077",
//    "gmail", "11ee", "matale", "111", "pagero", "engi", "sweden", "2323", 0, false, true, false)
//  val acc2 = Account(
//    "111", "222", "password", "user", List(), "pubkey", "device_token", "apple", "imei", "1", "2", "3", "8736", "eranga", "78", "077",
//    "gmail", "11ee", "matale", "111", "pagero", "engi", "sweden", "2323", 0, false, true, false)
//  CassandraStore.createAccount(acc1)
//  println(CassandraStore.getAccount("111", "222"))
//  CassandraStore.registerAccount(acc2)
//  println(CassandraStore.getAccount("111", "222"))
//  CassandraStore.activateAccount("111", "222", true)
//  CassandraStore.verifyAccount("111", "222", true)
//  //    CassandraStore.updateAttempts("775432015", 2)
//  //    CassandraStore.disableAccount("775432015", true)
//  CassandraStore.createTrans(Trans("112", "eranga", "AccountActor", "create", "{\"message\": \"java\"}", "1111"))
//
//  CassandraStore.createTrace(Trace("111", "111", "1212", "ops", "077", "222", "2323", "333", "hak", "matale", "232", "sig", false, 2.121, 4.2323))
//  println(CassandraStore.getTrace("111", "111"))
//
//  CassandraStore.createConsents(Consent("111", "111", "1212", "ops", "077", "222", "edd", "333", "hak", "matale"))
//  CassandraStore.createConsents(Consent("222", "111", "1212", "ops", "077", "222", "edd", "333", "hak", "matale"))
//  println(CassandraStore.getConsents("111"))
//}
