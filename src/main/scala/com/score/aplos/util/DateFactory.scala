package com.score.aplos.util

import java.text.SimpleDateFormat
import java.util.{Date, TimeZone}

object DateFactory {
  val COLOMBO_TIME_ZONE = TimeZone.getTimeZone("Asia/Colombo")
  val UTC_TIME_ZONE = TimeZone.getTimeZone("UTC")
  val TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS"
  val TIMESTAMP_FORMAT_E = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
  val DATE_FORMAT = "yyyy-MM-dd"

  def formatToDate(date: String, format: String, timeZone: TimeZone): Date = {
    // elassandra comes with utc timezone
    val sdf = new SimpleDateFormat(format)
    sdf.setTimeZone(timeZone)
    sdf.parse(date)
  }

  def formatToString(date: Option[Date], format: String, timeZone: TimeZone): Option[String] = {
    date match {
      case Some(d) =>
        val sdf = new SimpleDateFormat(format)
        sdf.setTimeZone(timeZone)
        Option(sdf.format(d))
      case None =>
        None
    }
  }

  def timestamp(): Long = {
    System.currentTimeMillis() / 1000
  }

  def timestamp(ttl: Option[Int] = None): Long = {
    ttl match {
      case Some(p) =>
        (System.currentTimeMillis() / 1000) + (p * 60 * 1000)
      case None =>
        System.currentTimeMillis() / 1000
    }
  }

  def expiredTimestamp(issueTime: Long, ttl: Long): Boolean = {
    issueTime + (ttl * 60 * 1000) >= System.currentTimeMillis() / 1000
  }
}
