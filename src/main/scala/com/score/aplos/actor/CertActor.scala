package com.score.aplos.actor

import akka.actor.{Actor, Props}
import akka.stream.ActorMaterializer
import com.score.aplos.actor.CertActor.{Create, Get}
import com.score.aplos.cassandra.{CassandraStore, Cert}
import com.score.aplos.config.AppConf
import com.score.aplos.protocol._
import com.score.aplos.redis.RedisStore
import com.score.aplos.util.{AppLogger, CryptoFactory, DateFactory}

object CertActor {

  case class Create(messageType: String, execer: String, id: String,
                    certId: String,
                    certPubKey: String) extends CertMessage

  case class Get(messageType: String, execer: String, id: String,
                 certId: String) extends CertMessage

  def props()(implicit materializer: ActorMaterializer) = Props(new CertActor)

}

class CertActor()(implicit materializer: ActorMaterializer) extends Actor with AppConf with AppLogger {
  override def receive: Receive = {
    case create: Create =>
      logger.info(s"got create message - $create")

      // first check double spend
      val id = s"${create.execer};CertActor;${create.id}"
      if (!CassandraStore.isDoubleSpend(create.execer, "CertActor", create.id) && RedisStore.set(id)) {
        // create cert
        val digsig = CryptoFactory.sign(create.certPubKey)
        CassandraStore.createCert(Cert(create.certId, create.certPubKey, digsig))

        logger.info(s"cert created - $create")
        sender ! StatusReply(201, "Created")
      } else {
        logger.error(s"double spend update $create")

        sender ! StatusReply(400, "double spend")
      }

      context.stop(self)
    case get: Get =>
      logger.info(s"got get message - $get")

      // get cert
      CassandraStore.getCert(get.certId) match {
        case Some(c) =>
          sender ! CertReply(c.id, c.pubkey, c.digsig,
            DateFactory.formatToString(Option(c.timestamp), DateFactory.TIMESTAMP_FORMAT, DateFactory.COLOMBO_TIME_ZONE).getOrElse(""))
        case _ =>
          logger.error(s"no matching cert found - ${get.certId}")
          sender ! StatusReply(404, "Failed to get cert, cert not found")
      }

      context.stop(self)
  }

}

