package com.score.aplos.actor

import akka.actor.{Actor, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.pattern.ask
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}
import akka.util.Timeout
import com.score.aplos.actor.HttpActor.Serve
import com.score.aplos.config.AppConf
import com.score.aplos.protocol._
import com.score.aplos.util.AppLogger

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object HttpActor {

  case class Serve()

  def props()(implicit system: ActorSystem) = Props(new HttpActor)

}

class HttpActor()(implicit system: ActorSystem) extends Actor with AppLogger with AppConf {

  override def receive: Receive = {
    case Serve =>
      // supervision
      // meterializer for streams
      val decider: Supervision.Decider = { e =>
        logError(e)
        Supervision.Resume
      }
      implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))
      implicit val ec = system.dispatcher

      Http().bindAndHandle(route, "0.0.0.0", servicePort)
  }

  def route()(implicit materializer: ActorMaterializer, ec: ExecutionContextExecutor) = {
    implicit val timeout = Timeout(90.seconds)

    import com.score.aplos.protocol.CertMessageProtocol._
    import com.score.aplos.protocol.StatusReplyProtocol._

    pathPrefix("api") {
      path("certs") {
        get {
          complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "cert contract"))
        } ~
          post {
            entity(as[CertMessage]) {
              case create: CertActor.Create =>
                val f = context.actorOf(CertActor.props()) ? create
                onComplete(f) {
                  case Success(status: StatusReply) =>
                    logger.info(s"create status $status")
                    complete(if (status.code == 201) StatusCodes.Created -> status else StatusCodes.BadRequest -> status)
                  case Failure(e) =>
                    logError(e)
                    complete(StatusCodes.BadRequest -> StatusReply(400, "Failed to complete the request"))
                  case _ =>
                    logger.info("fail create cert")
                    complete(StatusCodes.BadRequest -> StatusReply(400, "Failed to complete the request"))
                }
              case get: CertActor.Get =>
                val f = context.actorOf(CertActor.props()) ? get
                onComplete(f) {
                  case Success(reply: CertReply) =>
                    logger.debug(s"cert reply $reply")
                    import com.score.aplos.protocol.CertReplyProtocol._
                    complete(StatusCodes.OK -> reply)
                  case Success(status: StatusReply) =>
                    logger.info(s"get cert status $status")
                    complete(if (status.code == 404) StatusCodes.NotFound else StatusCodes.BadRequest -> status)
                  case Failure(e) =>
                    logError(e)
                    complete(StatusCodes.BadRequest -> StatusReply(400, "Failed to complete the request"))
                  case _ =>
                    logger.info("fail to get cert")
                    complete(StatusCodes.BadRequest -> StatusReply(400, "Failed to complete the request"))
                }
            }
          }
      }
    }
  }
}
