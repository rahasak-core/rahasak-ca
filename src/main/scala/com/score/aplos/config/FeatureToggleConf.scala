package com.score.aplos.config

import com.typesafe.config.ConfigFactory

import scala.util.Try

trait FeatureToggleConf {
  // config object
  val featureToggleCong = ConfigFactory.load("feature-toggle.conf")

  lazy val useSendSms = Try(featureToggleCong.getBoolean("feature-toggle.use-send-sms")).getOrElse(true)
}