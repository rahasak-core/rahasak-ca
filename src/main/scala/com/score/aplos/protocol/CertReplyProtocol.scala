package com.score.aplos.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class CertReply(id: String, payload: String, digsig: String, timestamp: String)

object CertReplyProtocol extends SprayJsonSupport with DefaultJsonProtocol {

  implicit val format = jsonFormat4(CertReply)

}

