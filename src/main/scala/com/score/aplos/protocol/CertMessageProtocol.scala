package com.score.aplos.protocol

import com.score.aplos.actor.CertActor._
import spray.json._

trait CertMessage

object CertMessageProtocol extends DefaultJsonProtocol {

  implicit val createFormat: JsonFormat[Create] = jsonFormat5(Create)
  implicit val getFormat: JsonFormat[Get] = jsonFormat4(Get)

  implicit object CertMessageFormat extends RootJsonFormat[CertMessage] {
    def write(obj: CertMessage): JsValue =
      JsObject((obj match {
        case c: Create => c.toJson
        case g: Get => g.toJson
        case unknown => deserializationError(s"json deserialize error: $unknown")
      }).asJsObject.fields)

    def read(json: JsValue): CertMessage =
      json.asJsObject.getFields("messageType") match {
        case Seq(JsString("create")) => json.convertTo[Create]
        case Seq(JsString("get")) => json.convertTo[Get]
        case unrecognized => serializationError(s"json serialization error $unrecognized")
      }
  }

}
